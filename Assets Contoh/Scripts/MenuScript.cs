﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour 
{
	//Initializing a laser for touch engine//
	private Ray ray;
	private RaycastHit rayCastHit;

	//Variable for knowing which state of menu in current time
	private bool animComplete;
	private bool hasntFade;
    // TODO: Ganti jadi enum, jangan string
	private string menuState = "";


	private string playerName = "";

	//Variable for storing GameObject on MenuScreen
	public GameObject ball1;
	public GameObject ball2;
	public GameObject ball3;
	public GameObject ball4;
	
    public GameObject aboutGameButton;
	public GameObject exitGameButton;
	public GameObject P1GameButton;
	public GameObject P2GameButton;

	public GameObject scriptManager;
	public GUIText yourName;
	public GUIText highScorePlayer;
	public GameObject delayController;


	void Start () 
	{
		Debug.Log("MenuScript Initialized");
		Time.timeScale = 1.0f;
		iTween.MoveBy(P1GameButton, iTween.Hash("y", -8f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "time", 2));
		iTween.MoveBy(P2GameButton, iTween.Hash("x", -8f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "time", 2));
		iTween.MoveBy(aboutGameButton, iTween.Hash("y", 8f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "time", 2));
		iTween.MoveBy(exitGameButton, iTween.Hash("x", 8f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "time", 2));
//		hasFade = false;
//		P1GameButton.collider.enabled = true;
//		P2GameButton.collider.enabled = true;
//		aboutGameButton.collider.enabled = true;
//		exitGameButton.collider.enabled = true;
	}


	void OnGUI()
	{
		if(animComplete && menuState == "1P")
		{
			yourName.text = "Input Your Name";
			if(PlayerPrefs.GetString("HighScorePlayer") == "")
				highScorePlayer.text = "Highscore \n 0";
			else
				highScorePlayer.text = "Highscore by " + PlayerPrefs.GetString("HighScorePlayer") + "\n" + PlayerPrefs.GetInt("HighScore").ToString();
			playerName = GUI.TextField(new Rect(Screen.width/3.2f,Screen.height*0.35f,Screen.width*0.4f,Screen.height*0.05f), playerName,10);
			if(GUI.Button(new Rect(Screen.width/2.6f,Screen.height*0.45f,Screen.width*0.25f,Screen.height*0.05f), "Play"))
			{ ;
				if(playerName == "")
					PlayerPrefs.SetString("PlayerName", "Guest");
				else
					PlayerPrefs.SetString("PlayerName", playerName);
				Application.LoadLevel("1PlayerGame");
			}
		}
		if(animComplete & menuState == "About")
		{
			Debug.Log("about apps");
			string about;
			about = "This is PONG games ! \n\n You miss your time when you play pong in\n arcade ? don't worry we'll give you those\n memories !\n\nPress the oval to summon your player\nand move it to left or right.\nDon't let the ball pass and get your name\nin glorious highscore !\n\n\nThere's 2Player DeadMatch Mode too !\ntest your concentration with your friend.\nwhoever let the ball passes first\nis a loser !\n\n\n\nCreated By :\nAnjar Aditya Pratama (@adittt_tttida)\nFacebook.com/bloodyadit\n\n\n\nSpecial Thanks to:\nITween Library\nwww.1001freefonts.com/\nRifal Mafaza (@rivalfaza)";
			GUI.Label(new Rect(Screen.width/8.0f,Screen.height/8.0f,Screen.width/1.3f,Screen.height/1.3f), about, "TextField");
			if(Input.GetKeyDown(KeyCode.Escape))
				AnimateAboutBack();
		}
	}


	void Update () 
	{
		AnimateBall();

		if(Input.GetMouseButtonDown(0))
		{
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if(Physics.Raycast(ray, out rayCastHit))
			{
				Debug.Log("raycast hit !");
				Transform rayCastTrans = rayCastHit.transform;
				if(rayCastTrans.name == "btn1Play")
				{
					_1PAction();
				}
				else if(rayCastTrans.name == "btn2Play")
				{
					_2PAction();
				}
				else if(rayCastTrans.name == "btnAbout" && menuState != "About")
				{
					AboutAction();
				}
				else if(rayCastTrans.name == "btnExit")
				{
					QuitAction();
				}
			}
		}

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(menuState == "1P" && DisableKey.buttonEnabled)
			{
				Animate1PPlayBack();
			}
			else if(menuState == "About" && DisableKey.buttonEnabled)
			{
				AnimateAboutBack();
			}
			else if(menuState == "" && DisableKey.buttonEnabled)
			{
				QuitAction();
			}
		}
	}


	//<-----------------------------Action Pack---------------------------->//

	void _1PAction()
	{
		DisableKey.buttonEnabled = false;
		Debug.Log("Accessing 1PGame Mode");
		hasntFade = true;
		menuState = "1P";
		iTween.MoveBy(P1GameButton, iTween.Hash("y", 1f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "AnimComplete", "oncompletetarget", scriptManager));
		iTween.MoveBy(P2GameButton, iTween.Hash("y", -2.5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "_2PPlayFade", "oncompletetarget", scriptManager));
		iTween.MoveBy(aboutGameButton, iTween.Hash("y", -2.5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "AboutFade", "oncompletetarget", scriptManager));
		iTween.MoveBy(exitGameButton, iTween.Hash("y", -2.5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "ExitFade", "oncompletetarget", scriptManager));
		//disabling control, enabling button.
		iTween.MoveBy(delayController, iTween.Hash("y", -.5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "DisableMenuItems", "oncompletetarget", scriptManager));
	}

	void Animate1PPlayBack()
	{
		DisableKey.buttonEnabled = false;
		Debug.Log("Accessing MainMenu");
		animComplete = false;
		hasntFade = false;
		iTween.MoveBy(P1GameButton, iTween.Hash("y", -1f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "EnableMenuItems", "oncompletetarget", scriptManager));
		iTween.MoveBy(P2GameButton, iTween.Hash("y", 2.5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "_2PPlayFade", "oncompletetarget", scriptManager));
		iTween.MoveBy(aboutGameButton, iTween.Hash("y", 2.5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "AboutFade", "oncompletetarget", scriptManager));
		iTween.MoveBy(exitGameButton, iTween.Hash("y", 2.5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "ExitFade", "oncompletetarget", scriptManager));
		yourName.text = "";
		highScorePlayer.text = "";
		ResetMenuState();
	}

	void _2PAction()
	{
		Application.LoadLevel("2PlayerGame");
	}

	void AboutAction()
	{
		DisableKey.buttonEnabled = false;
		Debug.Log("Accessing About Menu");
		hasntFade = true;
		menuState = "About";
		iTween.MoveBy(P1GameButton, iTween.Hash("y", 5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "AnimComplete", "oncompletetarget", scriptManager));
		iTween.MoveBy(P2GameButton, iTween.Hash("y", 5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f));
		iTween.MoveBy(aboutGameButton, iTween.Hash("y", 4.5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f));
		iTween.MoveBy(exitGameButton, iTween.Hash("y", -5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f));
		//disabling control, enabling button.
		iTween.MoveBy(delayController, iTween.Hash("y", .5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "DisableMenuItems", "oncompletetarget", scriptManager));
	}

	void AnimateAboutBack()
	{
		Debug.Log("Accessing MainMenu");
		animComplete = false;
		hasntFade = true;
		iTween.MoveBy(P1GameButton, iTween.Hash("y", -5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f));
		iTween.MoveBy(P2GameButton, iTween.Hash("y", -5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f));
		iTween.MoveBy(aboutGameButton, iTween.Hash("y", -4.5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "ResetMenuState", "oncompletetarget", scriptManager));
		iTween.MoveBy(exitGameButton, iTween.Hash("y", 5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "EnableMenuItems", "oncompletetarget", scriptManager));
	}

	void QuitAction()
	{
		iTween.MoveBy(P1GameButton, iTween.Hash("y", 8f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "time", .777f));
		iTween.MoveBy(P2GameButton, iTween.Hash("x", 8f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "time", .777f));
		iTween.MoveBy(aboutGameButton, iTween.Hash("y", -8f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "time", .777f));
		iTween.MoveBy(exitGameButton, iTween.Hash("x", -8f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "time", .777f, "onComplete", "QuitGame", "oncompletetarget", scriptManager));
	}

	void QuitGame()
	{
		Debug.Log("quitting game..");
		Application.Quit();
	}

	//<-----------------------------End Of Action Pack---------------------------->//


	

	//<-----------------------------Fading Menu Item---------------------------->//
	void _2PPlayFade()
	{
		if(hasntFade)
		{
			iTween.FadeTo(P2GameButton, iTween.Hash("alpha",0.1f,"time",0.1f));
			iTween.FadeTo(ball2, iTween.Hash("alpha",0.1f,"time",0.1f));
		}
		else
		{
			iTween.FadeTo(P2GameButton, iTween.Hash("alpha",1,"time",1));
			iTween.FadeTo(ball2, iTween.Hash("alpha",1,"time",1));
		}
	}

	void AboutFade()
	{
		if(hasntFade)
		{
			iTween.FadeTo(aboutGameButton, iTween.Hash("alpha",0.1f,"time",0.1f));
			iTween.FadeTo(ball3, iTween.Hash("alpha",0.1f,"time",0.1f));
		}
		else
		{
			iTween.FadeTo(aboutGameButton, iTween.Hash("alpha",1,"time",1));
			iTween.FadeTo(ball3, iTween.Hash("alpha",1,"time",1));
		}
	}

	void ExitFade()
	{
		if(hasntFade)
		{
			iTween.FadeTo(exitGameButton, iTween.Hash("alpha",0.1f,"time",0.1f));
			iTween.FadeTo(ball4, iTween.Hash("alpha",0.1f,"time",0.1f));
		}
		else
		{
			iTween.FadeTo(exitGameButton, iTween.Hash("alpha",1,"time",1));
			iTween.FadeTo(ball4, iTween.Hash("alpha",1,"time",1));
		}
	}
	//<-----------------------------End Of Fading Menu Item---------------------------->//


	//<-----------------------------Miscellaneous Code Pack---------------------------->//
	void AnimateBall()
	{
		ball1.gameObject.transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime * 4);
		ball2.gameObject.transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime * 4);
		ball3.gameObject.transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime * 4);
		ball4.gameObject.transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime * 4);
	}

	void DisableMenuItems()
	{
		P1GameButton.collider.enabled = false;
		P2GameButton.collider.enabled = false;
		aboutGameButton.collider.enabled = false;
		exitGameButton.collider.enabled = false;
		DisableKey.buttonEnabled = true;
	}
	
	void EnableMenuItems()
	{
		P1GameButton.collider.enabled = true;
		P2GameButton.collider.enabled = true;
		aboutGameButton.collider.enabled = true;
		exitGameButton.collider.enabled = true;
	}
	
	void AnimComplete()
	{
		//Change value of animComplete to true, animComplete is used for checking if animation is complete, if true, showing GUI
		animComplete = true;
	}
	
	void ResetMenuState()
	{
		menuState = "";
	}
	//<-----------------------------End Of Miscellaneous Code Pack---------------------------->//


	//	void EscapePlayState()
	//	{
	//		iTween.MoveBy(aboutGameButton, iTween.Hash("y", 2.5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "AboutFade", "oncompletetarget", scriptManager));
	//		iTween.MoveBy(exitGameButton, iTween.Hash("y", 2.5f, "easeType", "easeInOutExpo", "loopType", "none", "delay", .1f, "onComplete", "ExitFade", "oncompletetarget", scriptManager));
	//		GameMode();
	//		rayCastHitPlay = false;
	//		//menuState = "";
	//		hasFade = false;
	//		aboutGameButton.collider.enabled = true;
	//		exitGameButton.collider.enabled = true;
	//	}
	
	//	void GameMode()
	//	{
	//		PlayFade();
	//		if(P1GameButton.activeSelf == true)
	//		{
	//			P1GameButton.SetActive(false);
	//			P2GameButton.SetActive(false);
	//		}
	//		else
	//		{
	//			P1GameButton.SetActive(true);
	//			P2GameButton.SetActive(true);
	//		}
	//	}
}

class DisableKey
{
	public static bool buttonEnabled = true;
}
