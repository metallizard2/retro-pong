﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour 
{
	private Ray ray;
	private RaycastHit rayCastHit;
	public GameObject playerUp;
	public GameObject playerDown;

	// Use this for initialization
	void Start () 
	{
		Debug.Log("PlayerScript initialized");	
		playerUp.renderer.material.color = Color.green;
		playerDown.renderer.material.color = Color.green;
		GameObject.Find("PlaneUp").renderer.material.color = Color.red;
		GameObject.Find("PlaneDown").renderer.material.color = Color.blue;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetMouseButton(0))
		{
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if(Physics.Raycast(ray,out rayCastHit))
			{
				//initialized and logs

				if(rayCastHit.collider.tag == "AreaUp")
				{
					Debug.Log("raycast up hit !");
					float xRayPoint;
					float playerPosY;
					float playerPosZ;
					xRayPoint = rayCastHit.point.x;
					playerPosY = playerUp.transform.position.y;
					playerPosZ = playerUp.transform.position.z;
					Vector3 touchPosition = transform.position;
					float xRayValid = customCollide(xRayPoint);


					touchPosition.x = xRayValid;
					playerUp.transform.position = new Vector3(touchPosition.x,playerPosY,playerPosZ); 
				}
				else if(rayCastHit.collider.tag == "AreaDown")
				{
					Debug.Log("raycast down hit !");
					float xRayPoint;
					float playerPosY;
					float playerPosZ;
					xRayPoint = rayCastHit.point.x;
					playerPosY = playerDown.transform.position.y;
					playerPosZ = playerDown.transform.position.z;
					Vector3 touchPosition = transform.position;
					float xRayValid = customCollide(xRayPoint);

					touchPosition.x = xRayValid;
					playerDown.transform.position = new Vector3(touchPosition.x,playerPosY,playerPosZ); 
				}
			}
		}
	}

	private float customCollide(float x)
	{
		if(x < -2.4f)
		{
			x = -2.4f;
		}
		else if(x > 2.4f)
		{
			x = 2.4f;
		}
		return x;
	}
}
