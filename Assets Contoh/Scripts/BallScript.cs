﻿using UnityEngine;
using System.Collections;

public class BallScript : MonoBehaviour 
{
	public GameObject bola;
	public float xAxesSpeed;
	public float yAxesSpeed;
	public TextManager textMgr;
	public SinglePlayerScript singleScript;
	
	private int score = 0;

	[HideInInspector]
	public bool lose;

	[HideInInspector]
	public bool pause;

	void Start()
	{
		Time.timeScale = 1.0f;
		lose = false;
		pause = false;
	}

	void Awake()
	{
		bola.rigidbody.AddForce(xAxesSpeed,yAxesSpeed,0,ForceMode.Impulse);

		InvokeRepeating("IncreaseSpeedBall", 2, 2);
		InvokeRepeating("CountScore", 0.01f, 0.03f);
	}

	void IncreaseSpeedBall()
	{
		if(bola)
		{
			bola.rigidbody.velocity *= 1.03f;
			Debug.Log("current ball speed : " + bola.rigidbody.velocity);
		}

	}

	void CountScore()
	{
		score += 1;
	}

	public int GetScore()
	{
		return score;
	}

	void Update()
	{
		if(bola)
		{
			if(bola.gameObject.transform.position.y < -4.3f)
			{
				lose = true;
				CancelInvoke();
				Debug.Log("Score Stopped");
				textMgr.GameOver();
			}
		}

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			pause = !pause;
			if(pause & !lose)
			{
				Time.timeScale = 0.0f;
				singleScript.enabled = false;
				textMgr.Pause(pause);
			}
			else if(!lose)
			{
				Time.timeScale = 1.0f;
				singleScript.enabled = true;
				textMgr.Pause(pause);
			}
		}
	}

	void OnGUI()
	{
		if(lose)
		{
			if(textMgr.getStatusAnim())
			{
				if(GUI.Button(new Rect(Screen.width*0.25f,Screen.height*0.37f, Screen.width*0.5f,Screen.width*0.1f), "Play Again"))
				{
					lose = false;
					Application.LoadLevel("1PlayerGame");
				}
				if(GUI.Button(new Rect(Screen.width*0.25f,Screen.height*0.46f, Screen.width*0.5f,Screen.width*0.1f), "Back To Menu"))
				{
					Application.LoadLevel("MenuGame");
				}
			}
		}

		if(pause & !lose)
		{
			if(GUI.Button(new Rect(Screen.width*0.25f,Screen.height*0.48f, Screen.width*0.5f,Screen.width*0.1f), "Back To Menu"))
			{
				Application.LoadLevel("MenuGame");
			}
		}


	}

	

}
