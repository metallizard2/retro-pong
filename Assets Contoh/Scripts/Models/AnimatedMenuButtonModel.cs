﻿using UnityEngine;

namespace Assets.Scripts.Models
{
    public class AnimatedMenuButtonModel : BaseModel
    {
        public string Label { get; set; }

        public Vector3 Position { get; set; }


        public string Axis { get; set; }
        public float Distance { get; set; }
        public float BackDistance { get; set; }
    }
}
