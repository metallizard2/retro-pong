﻿using Assets.Scripts.Controller;
using Assets.Scripts.Models;
using UnityEngine;

namespace Assets.Scripts.Core
{
    public class PongContext : MonoBehaviour
    {
        public void Start()
        {
            var onePlayerButtonModel = new AnimatedMenuButtonModel();
            onePlayerButtonModel.Label = "1 Player";
            onePlayerButtonModel.Position = new Vector3(-1.35f, 10f, 0);
            onePlayerButtonModel.Axis = "y";
            onePlayerButtonModel.Distance = -8f;
            onePlayerButtonModel.BackDistance = 1f;

            var onePlayerButton = new AnimatedMenuButton(onePlayerButtonModel, gameObject);
            onePlayerButton.InstantiateButton();


            var twoPlayerButtonModel = new AnimatedMenuButtonModel();
            twoPlayerButtonModel.Label = "2 Player";
            twoPlayerButtonModel.Position = new Vector3(6.65f, 1f, 0);
            twoPlayerButtonModel.Axis = "x";
            twoPlayerButtonModel.Distance = -8f;
            twoPlayerButtonModel.BackDistance = -2.5f;

            var twoPlayerButton = new AnimatedMenuButton(twoPlayerButtonModel, gameObject);
            twoPlayerButton.InstantiateButton();

            onePlayerButton.StartAnimation();
            twoPlayerButton.StartAnimation();
        }
    }
}
