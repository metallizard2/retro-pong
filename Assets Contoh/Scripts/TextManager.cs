﻿using UnityEngine;
using System.Collections;

public class TextManager : MonoBehaviour 
{
	public GameObject pause;
	public GameObject Lose;
	public GUIText Score;
	public GUIText HighScore;
	public BallScript ballScript;
	public GameObject textMgr;

	private bool animComplete;
	private bool y = false;
	// Use this for initialization
	void Start () 
	{
		if(PlayerPrefs.GetInt("HighScore") <= 0)
		{
			HighScore.text = "HighScore : 0";
		}
		else
		{
			HighScore.text = "HighScore :" + PlayerPrefs.GetInt("HighScore").ToString();
		}
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		Score.text = PlayerPrefs.GetString("PlayerName") + "'s Score : " + ballScript.GetScore().ToString();
	}

	public void GameOver()
	{
		if(!y)
		{
			Handheld.Vibrate();
			Lose.gameObject.guiText.text = "YOU LOSE"; 
			iTween.MoveBy(Lose, iTween.Hash("y", 0.2f, "easeType", "easeInOutExpo", "loopType", "none", "delay", 1.5f,"time",1.5f, "oncomplete", "AnimComplete", "oncompletetarget", textMgr));
			y = true;
		}
	}

	public void Pause(bool x)
	{
		if(x)
		{
			pause.gameObject.guiText.text = "Paused";
		}
		else
		{
			pause.gameObject.guiText.text = "";
		}
	}

	void AnimComplete()
	{
		Debug.Log("animcomplete textmanager");
		animComplete = true;
	}

	public bool getStatusAnim()
	{
		return animComplete;
	}
}
