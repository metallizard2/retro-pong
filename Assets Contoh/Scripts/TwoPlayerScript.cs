﻿using UnityEngine;
using System.Collections;

public class TwoPlayerScript : MonoBehaviour 
{
	private Ray ray;
	private Ray ray2;
	private RaycastHit rayCastHit;
	private RaycastHit rayCastHit2;

	public GameObject playerUp;
	public GameObject controllerUp;
	public GameObject playerDown;
	public GameObject controllerDown;
	public BallScript2 ballScript;

	// Use this for initialization
	void Start () 
	{
		Debug.Log("PlayerScript initialized");	
		playerDown.renderer.material.color = Color.green;
		playerUp.renderer.material.color = Color.red;
	}
	
	// Update is called once per frame
	void Update () 
	{
		BoxCollider controllerColliderUp = controllerUp.collider as BoxCollider;
		BoxCollider controllerColliderDown = controllerDown.collider as BoxCollider;

		if(Input.touches.Length == 1)
		{
			ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

			if(Physics.Raycast(ray,out rayCastHit))//there's 1 touch in screen colliding with something
			{
				if(rayCastHit.collider.tag == "AreaDown")// #1 touch collide with areaDown
				{
					Debug.Log("raycast down hit !");
					controllerColliderDown.size = new Vector3(5,5,1);
					iTween.ScaleTo(playerDown,new Vector3(2.0f,0.3f,1.0f),0.5f);
					float xRayPoint;
					float playerPosY;
					float playerPosZ;
					xRayPoint = rayCastHit.point.x;
					playerPosY = playerDown.transform.position.y;
					playerPosZ = playerDown.transform.position.z;
					Vector3 touchPosition = transform.position;
					float xRayValid = customCollide(xRayPoint);

					touchPosition.x = xRayValid;
					controllerDown.transform.position = new Vector3(touchPosition.x,controllerDown.transform.position.y,controllerDown.transform.position.z); 
					playerDown.transform.position = new Vector3(touchPosition.x,playerPosY,playerPosZ); 

				}
				if(rayCastHit.collider.tag == "AreaUp")// #1 touch collide with areaUp
				{
					Debug.Log("raycast up hit !");
					controllerColliderUp.size = new Vector3(5,5,1);
					iTween.ScaleTo(playerUp,new Vector3(2.0f,0.3f,1.0f),0.5f);
					float xRayPoint;
					float playerPosY;
					float playerPosZ;
					xRayPoint = rayCastHit.point.x;
					playerPosY = playerUp.transform.position.y;
					playerPosZ = playerUp.transform.position.z;
					Vector3 touchPosition = transform.position;
					float xRayValid = customCollide(xRayPoint);
					
					touchPosition.x = xRayValid;
					controllerUp.transform.position = new Vector3(touchPosition.x,controllerUp.transform.position.y,controllerUp.transform.position.z); 
					playerUp.transform.position = new Vector3(touchPosition.x,playerPosY,playerPosZ); 
					
				}
			}
			else
			{
				controllerColliderDown.size = new Vector3(1.5f,1.5f,1);
				iTween.ScaleTo(playerDown, new Vector3(0,0,0), 0.3f);
			}
			// //
			ray2= Camera.main.ScreenPointToRay(Input.GetTouch(1).position);
			if(Physics.Raycast(ray2,out rayCastHit2))//there's 1 touch in screen colliding with something
			{
				if(rayCastHit2.collider.tag == "AreaDown")// #1 touch collide with areaDown
				{
					Debug.Log("raycast down hit !");
					iTween.ScaleTo(playerDown,new Vector3(2.0f,0.3f,1.0f),0.5f);
					float xRayPoint;
					float playerPosY;
					float playerPosZ;
					xRayPoint = rayCastHit2.point.x;
					playerPosY = playerDown.transform.position.y;
					playerPosZ = playerDown.transform.position.z;
					Vector3 touchPosition = transform.position;
					float xRayValid = customCollide(xRayPoint);
					
					touchPosition.x = xRayValid;
					controllerDown.transform.position = new Vector3(touchPosition.x,controllerDown.transform.position.y,controllerDown.transform.position.z); 
					playerDown.transform.position = new Vector3(touchPosition.x,playerPosY,playerPosZ); 
					
				}
				if(rayCastHit2.collider.tag == "AreaUp")// #1 touch collide with areaUp
				{
					Debug.Log("raycast up hit !");
					iTween.ScaleTo(playerUp,new Vector3(2.0f,0.3f,1.0f),0.5f);
					float xRayPoint;
					float playerPosY;
					float playerPosZ;
					xRayPoint = rayCastHit2.point.x;
					playerPosY = playerUp.transform.position.y;
					playerPosZ = playerUp.transform.position.z;
					Vector3 touchPosition = transform.position;
					float xRayValid = customCollide(xRayPoint);
					
					touchPosition.x = xRayValid;
					controllerUp.transform.position = new Vector3(touchPosition.x,controllerUp.transform.position.y,controllerUp.transform.position.z); 
					playerUp.transform.position = new Vector3(touchPosition.x,playerPosY,playerPosZ); 
					
				}
			}
		}
		else if(Input.touches.Length >= 2)
		{
			ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
			
			
			if(Physics.Raycast(ray,out rayCastHit))//there's 1 touch in screen colliding with something
			{
				if(rayCastHit.collider.tag == "AreaDown")// #1 touch collide with areaDown
				{
					Debug.Log("raycast down hit !");
					iTween.ScaleTo(playerDown,new Vector3(2.0f,0.3f,1.0f),0.5f);
					float xRayPoint;
					float playerPosY;
					float playerPosZ;
					xRayPoint = rayCastHit.point.x;
					playerPosY = playerDown.transform.position.y;
					playerPosZ = playerDown.transform.position.z;
					Vector3 touchPosition = transform.position;
					float xRayValid = customCollide(xRayPoint);
					
					touchPosition.x = xRayValid;
					controllerDown.transform.position = new Vector3(touchPosition.x,controllerDown.transform.position.y,controllerDown.transform.position.z); 
					playerDown.transform.position = new Vector3(touchPosition.x,playerPosY,playerPosZ); 
					
				}
				if(rayCastHit.collider.tag == "AreaUp")// #1 touch collide with areaUp
				{
					Debug.Log("raycast up hit !");
					iTween.ScaleTo(playerUp,new Vector3(2.0f,0.3f,1.0f),0.5f);
					float xRayPoint;
					float playerPosY;
					float playerPosZ;
					xRayPoint = rayCastHit.point.x;
					playerPosY = playerUp.transform.position.y;
					playerPosZ = playerUp.transform.position.z;
					Vector3 touchPosition = transform.position;
					float xRayValid = customCollide(xRayPoint);
					
					touchPosition.x = xRayValid;
					controllerUp.transform.position = new Vector3(touchPosition.x,controllerUp.transform.position.y,controllerUp.transform.position.z); 
					playerUp.transform.position = new Vector3(touchPosition.x,playerPosY,playerPosZ); 
					
				}
			}

			// //
			ray2= Camera.main.ScreenPointToRay(Input.GetTouch(1).position);
			if(Physics.Raycast(ray2,out rayCastHit2))//there's 1 touch in screen colliding with something
			{
				if(rayCastHit2.collider.tag == "AreaDown")// #1 touch collide with areaDown
				{
					Debug.Log("raycast down hit !");
					iTween.ScaleTo(playerDown,new Vector3(2.0f,0.3f,1.0f),0.5f);
					float xRayPoint;
					float playerPosY;
					float playerPosZ;
					xRayPoint = rayCastHit2.point.x;
					playerPosY = playerDown.transform.position.y;
					playerPosZ = playerDown.transform.position.z;
					Vector3 touchPosition = transform.position;
					float xRayValid = customCollide(xRayPoint);
					
					touchPosition.x = xRayValid;
					controllerDown.transform.position = new Vector3(touchPosition.x,controllerDown.transform.position.y,controllerDown.transform.position.z); 
					playerDown.transform.position = new Vector3(touchPosition.x,playerPosY,playerPosZ); 
					
				}
				if(rayCastHit2.collider.tag == "AreaUp")// #1 touch collide with areaUp
				{
					Debug.Log("raycast up hit !");
					iTween.ScaleTo(playerUp,new Vector3(2.0f,0.3f,1.0f),0.5f);
					float xRayPoint;
					float playerPosY;
					float playerPosZ;
					xRayPoint = rayCastHit2.point.x;
					playerPosY = playerUp.transform.position.y;
					playerPosZ = playerUp.transform.position.z;
					Vector3 touchPosition = transform.position;
					float xRayValid = customCollide(xRayPoint);
					
					touchPosition.x = xRayValid;
					controllerUp.transform.position = new Vector3(touchPosition.x,controllerUp.transform.position.y,controllerUp.transform.position.z); 
					playerUp.transform.position = new Vector3(touchPosition.x,playerPosY,playerPosZ); 
					
				}
			}
		}
		else
		{
			iTween.ScaleTo(playerDown, new Vector3(0,0,0), 0.3f);
			iTween.ScaleTo(playerUp, new Vector3(0,0,0), 0.3f);
		}

//		if(ballScript.p1Point)
//		{
//
//		}
//		if(ballScript.p2Point)
//		{
//
//		}
	}
	

	private float customCollide(float x)
	{
		if(x < -2.0f)
		{
			x = -2.0f;
		}
		else if(x > 2.0f)
		{
			x = 2.0f;
		}
		return x;
	}



}
