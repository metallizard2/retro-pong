﻿using UnityEngine;
using System.Collections;

public class TwoPTextManager : MonoBehaviour
{
	public GameObject pause;
	public GameObject Lose;
	public GUIText countStart;
	public BallScript2 ballScript;
	public GameObject textMgr;
	
	private bool animComplete;
	private bool y = false;

	float startTime;
	// Use this for initialization

	void Awake()
	{
		startTime = Time.time;
	}

	void OnGUI()
	{
		float guiTime = Time.time - startTime;

		float seconds = guiTime % 60;

		if(seconds >0 && seconds <1.5f)
		{
			countStart.text = "Ready";
		}
		if(seconds >1.5f && seconds <2.5f)
		{
			countStart.text = "3";
		}
		if(seconds >2.5 && seconds <3.5f)
		{
			countStart.text = "2";
		}
		if(seconds >3.5f && seconds <4.5f)
		{
			countStart.text = "1";
		}
		if(seconds >4.5f)
		{
			countStart.text = "";
			if(ballScript.bola.gameObject != null)
			{
				if(ballScript.bola.activeSelf == false)
				{
					ballScript.bola.SetActive(true);
				}
			}
		}
	}
	
	public void GameOver()
	{
		if(!y)
		{
			Handheld.Vibrate();
			Lose.gameObject.guiText.text = "YOU LOSE"; 
			iTween.MoveBy(Lose, iTween.Hash("y", 0.2f, "easeType", "easeInOutExpo", "loopType", "none", "delay", 1.5f,"time",1.5f, "oncomplete", "AnimComplete", "oncompletetarget", textMgr));
			y = true;
		}
	}
	
	public void Pause(bool x)
	{
		if(x)
		{
			pause.gameObject.guiText.text = "Paused";
		}
		else
		{
			pause.gameObject.guiText.text = "";
		}
	}
	
	void AnimComplete()
	{
		Debug.Log("animcomplete textmanager");
		animComplete = true;
	}
	
	public bool getStatusAnim()
	{
		return animComplete;
	}
}
