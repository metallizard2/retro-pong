﻿using UnityEngine;

namespace Assets.Scripts.Views
{
    public class BallRotationController : MonoBehaviour
    {
        private Transform _myTransform;

        public void Start()
        {
            _myTransform = transform;
        }
        public void Update()
        {
            _myTransform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime * 4);
        }
    }
}
