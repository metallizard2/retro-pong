﻿using UnityEngine;
using System.Collections;

public class TouchControl : TouchEngine 
{
	private Ray ray;
	private RaycastHit rayCastHit;

	public GameObject player;
	public GameObject controller;
	
	void Awake()
	{
		Debug.Log("Awake Initialized");
	}
	

	void Start () 
	{
		Debug.Log("TouchControl initialized");
	}

	void OnTouchFound()
	{
		iTween.ScaleTo(gameObject,new Vector3(2.0f,0.3f,1.0f),0.5f);
		float xRayPoint;
		float playerPosY;
		float playerPosZ;
		xRayPoint = rayCastHit.point.x;
		playerPosY = gameObject.transform.position.y;
		playerPosZ = gameObject.transform.position.z;
		Vector3 touchPosition = transform.position;
		float xRayValid = customCollide(xRayPoint);
		
		touchPosition.x = xRayValid;
		controller.transform.position = new Vector3(touchPosition.x,controller.transform.position.y,controller.transform.position.z); 
		player.transform.position = new Vector3(touchPosition.x,playerPosY,playerPosZ); 
	}

	void OnTouchLost()
	{
		iTween.ScaleTo(gameObject, new Vector3(0,0,0), 0.3f);
		iTween.ScaleTo(gameObject, new Vector3(0,0,0), 0.3f);
	}

	private float customCollide(float x)
	{
		if(x < -2.0f)
		{
			x = -2.0f;
		}
		else if(x > 2.0f)
		{
			x = 2.0f;
		}
		return x;
	}
}
