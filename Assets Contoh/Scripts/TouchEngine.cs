﻿using UnityEngine;
using System.Collections;

public class TouchEngine : MonoBehaviour 
{

	
	// Update is called once per frame
	void Update () 
	{
		//checking touches on screen
		if(Input.touches.Length <= 0)
		{
			//no touches detected on screen

		}
		else
		{
			//detecting touch(es) on screen
			for(int i = 0; i < Input.touchCount; i++)
			{
				if(this.guiTexture.HitTest(Input.GetTouch(i).position))
				{
					if(Input.GetTouch(i).phase == TouchPhase.Began)
					{
						this.SendMessage("OnTouchFound");
					}
					if(Input.GetTouch(i).phase == TouchPhase.Ended)
					{
						this.SendMessage("OnTouchLost");
					}
				}
			}
		}
	}
}
