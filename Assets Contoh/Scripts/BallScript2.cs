﻿using UnityEngine;
using System.Collections;

public class BallScript2 : MonoBehaviour 
{
	public GameObject bola;
	//public Transform bolaSpawnPoint;
	public TwoPTextManager textMgr;
	public TwoPlayerScript twoPlayerScript;

	public Texture winTexture;
	public Texture loseTexture;
	
	[HideInInspector]
	public bool lose;
	[HideInInspector]
	public bool p1Point;
	[HideInInspector]
	public bool p2Point;
	[HideInInspector]
	public bool pause;
	
	void Start()
	{
		Time.timeScale = 1.0f;
		lose = false;
		pause = false;
		//Instantiate(bola, bolaSpawnPoint.position, bolaSpawnPoint.rotation);
	}
	
	void Awake()
	{
		Invoke("StartForce", 5f);
		InvokeRepeating("IncreaseSpeedBall", 6,2);
	}

	void StartForce()
	{
		float xAxesSpeed = Random.Range(0,2);
		float yAxesSpeed = Random.Range(0,2);
		if(xAxesSpeed == 0)
			xAxesSpeed = -4;
		else
			xAxesSpeed = 4;
		if(yAxesSpeed == 0)
			yAxesSpeed = -4;
		else
			yAxesSpeed = 4;
		bola.rigidbody.AddForce(xAxesSpeed,yAxesSpeed,0,ForceMode.Impulse);
	}

	
	void IncreaseSpeedBall()
	{
		if(bola)
		{
			bola.rigidbody.velocity *= 1.01f;
			Debug.Log("current ball speed : " + bola.rigidbody.velocity);
		}
		
	}
	
	void Update()
	{
		if(bola)
		{
			if(bola.gameObject.transform.position.y < -4.5f)
			{
				p2Point = true;
				CancelInvoke();
				lose = true;
				if(bola.gameObject.transform.position.y < -10.0f)
					Destroy (bola.gameObject);
			}
			else if(bola.gameObject.transform.position.y > 4.5f)
			{
				p1Point = true;
				CancelInvoke();
				lose = true;
				if(bola.gameObject.transform.position.y > 10.0f)
					Destroy (bola.gameObject);
			}

		}
		
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			pause = !pause;
			if(pause & !lose)
			{
				Time.timeScale = 0.0f;
				twoPlayerScript.enabled = false;
				textMgr.Pause(pause);

			}
			else if(!lose)
			{
				Time.timeScale = 1.0f;
				twoPlayerScript.enabled = true;
				textMgr.Pause(pause);
			}
		}

		if(p1Point)
		{
//			GameObject area = GameObject.FindGameObjectWithTag("PlaneUp");
//			area.renderer.material.color = Color.white;
			GameObject Score = GameObject.Find("Score");
			GameObject Score2 = GameObject.Find("Score 2");
			Score.gameObject.renderer.material.color = Color.white;
			Score.gameObject.renderer.material.mainTexture = winTexture;
			Score2.gameObject.renderer.material.color = Color.white;
			Score2.gameObject.renderer.material.mainTexture = loseTexture;
		}
		if(p2Point)
		{
//			GameObject area = GameObject.FindGameObjectWithTag("PlaneDown");
//			area.renderer.material.color = Color.white;
			GameObject Score = GameObject.Find("Score");
			GameObject Score2 = GameObject.Find("Score 2");
			Score.gameObject.renderer.material.color = Color.white;
			Score.gameObject.renderer.material.mainTexture = loseTexture;
			Score2.gameObject.renderer.material.color = Color.white;
			Score2.gameObject.renderer.material.mainTexture = winTexture;
		}
	}

	void OnGUI()
	{
		if(lose)
		{
			if(GUI.Button(new Rect(Screen.width*0.25f,Screen.height*0.41f, Screen.width*0.5f,Screen.width*0.1f), "Play Again"))
			{
				lose = false;
				Application.LoadLevel("2PlayerGame");
			}
			if(GUI.Button(new Rect(Screen.width*0.25f,Screen.height*0.52f, Screen.width*0.5f,Screen.width*0.1f), "Back To Menu"))
			{
				Application.LoadLevel("MenuGame");
			}
		}
		
		if(pause & !lose)
		{
			if(GUI.Button(new Rect(Screen.width*0.25f,Screen.height*0.59f, Screen.width*0.5f,Screen.width*0.1f), "Back To Menu"))
			{
				Application.LoadLevel("MenuGame");
			}
		}
		
		
	}
	
	
	
}
