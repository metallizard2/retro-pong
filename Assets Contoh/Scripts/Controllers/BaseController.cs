﻿using Assets.Scripts.Models;

namespace Assets.Scripts.Controller
{
    public class BaseController
    {
        private readonly BaseModel _model;
        public BaseController(BaseModel model)
        {
            _model = model;
        }
    }
}
