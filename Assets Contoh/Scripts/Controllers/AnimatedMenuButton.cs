﻿using Assets.Scripts.Models;
using UnityEngine;

namespace Assets.Scripts.Controller
{
    public class AnimatedMenuButton : BaseController
    {
        private readonly AnimatedMenuButtonModel _model;
        private readonly GameObject _scriptManager;

        public AnimatedMenuButton(AnimatedMenuButtonModel model, GameObject scriptManager) : base(model)
        {
            _model = model;
            _scriptManager = scriptManager;
        }

        private GameObject _gameObject;
        private GameObject _ballObject;
        public void InstantiateButton()
        {
            _gameObject = Object.Instantiate(Resources.Load<Object>("Button")) as GameObject;
            _gameObject.name = _model.Label;
            _gameObject.transform.position = _model.Position;
            var textMesh = _gameObject.GetComponent<TextMesh>().text = _model.Label;
        }

        public void StartAnimation()
        {
            iTween.MoveBy(_gameObject, 
                iTween.Hash(
                _model.Axis, _model.Distance, 
                "easeType", "easeInOutExpo", 
                "loopType", "none", 
                "delay", .1f, 
                "time", 2));
        }

        public void BackAnimation()
        {
            iTween.MoveBy(_gameObject, 
                iTween.Hash(
                _model.Axis, -_model.Distance, 
                "easeType", "easeInOutExpo", 
                "loopType", "none", 
                "delay", .1f, 
                "time", .777f));
        }

        private bool _isFaded;
        public void ToggleFade()
        {
            if (_isFaded)
            {
                iTween.FadeTo(_gameObject, iTween.Hash("alpha", 0.1f, "time", 0.1f));
                iTween.FadeTo(_ballObject, iTween.Hash("alpha", 0.1f, "time", 0.1f));
            }
            else
            {
                iTween.FadeTo(_gameObject, iTween.Hash("alpha", 1, "time", 1));
                iTween.FadeTo(_ballObject, iTween.Hash("alpha", 1, "time", 1));
            }
        }

        public void OnMouseHit()
        {
            
        }
    }
}
