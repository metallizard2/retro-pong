﻿using UnityEngine;
using System.Collections;

public class SinglePlayerScript : MonoBehaviour 
{
	private Ray ray;
	private RaycastHit rayCastHit;
	private string playerName;

	public GameObject playerDown;
	public GameObject controller;
	public GameObject singlePlayerScript;
	public BallScript ballScript;
	
	void Start () 
	{
		Debug.Log("PlayerScript initialized");	
		playerDown.renderer.material.color = Color.green;
		playerName = PlayerPrefs.GetString("PlayerName");
	}

	void Update () 
	{
		//if(Input.touches.Length >=1)
		if(Input.GetMouseButton(0))
		{
			//ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(ray,out rayCastHit))
			{
				//player touching "AreaDown" tag
				if(rayCastHit.collider.tag == "AreaDown")
				{
					Debug.Log("raycast down hit !");
					ExpandPlayer(1,1);
					PlayerController();
				}
				//player not touching "Areadown" but still at "touch"
				else
				{
					//<---------------------Do Nothing--------------------->//
				}
			}
		}
		else
		{
			ExpandPlayer(0,0);
		}

		if(ballScript.lose)
		{
			if(ballScript.GetScore() > PlayerPrefs.GetInt("HighScore"))
			{
				Debug.Log("New HighScore !");
				PlayerPrefs.SetString("HighScorePlayer", playerName);
				PlayerPrefs.SetInt("HighScore", ballScript.GetScore());
				Debug.Log("HighScorePlayer : "+PlayerPrefs.GetString("HighScorePlayer ") + PlayerPrefs.GetInt("HighScore").ToString());
			}
		}
	}

	//<------------------------------------------------------------------ Player Function --------------------------------------------------------------->//

	/*<----------------------------------------------------------Explanation of ExpandPlayer function---------------------------------------------------------->
	when controllerstatus is 1 expanding box collider, 0 for reset it. when playerStatus is 1 activate the player, 0 for deactivating player*/
	void ExpandPlayer(int controllerStatus, int playerStatus)
	{
		BoxCollider controllerCollider = controller.collider as BoxCollider;
		Hashtable hashPlayer = new Hashtable();
		Vector3 playerOn = new Vector3(2.0f,0.3f,1.0f);
		Vector3 playerOff = new Vector3(0,0,0);

		if(controllerStatus == 1)
		{
			controllerCollider.size = new Vector3(5,5,1);
		}
		else if(controllerStatus == 0)
		{
			controllerCollider.size = new Vector3(1.5f,1.5f,1);
		}

		if(playerStatus == 1)
		{
			hashPlayer.Add("onstart", "ActivatePlayer");
			hashPlayer.Add("onstarttarget", singlePlayerScript);
			hashPlayer.Add("scale", playerOn);
			iTween.ScaleTo(playerDown, hashPlayer);
		}
		else if(playerStatus == 0)
		{
			hashPlayer.Add("oncomplete", "DeactivatePlayer");
			hashPlayer.Add("oncompletetarget", singlePlayerScript);
			hashPlayer.Add("scale", playerOff);
			iTween.ScaleTo(playerDown, hashPlayer);
		}
	}

	void PlayerController()
	{
		float xRayPoint;
		float playerPosY;
		float playerPosZ;
		xRayPoint = rayCastHit.point.x;
		playerPosY = playerDown.transform.position.y;
		playerPosZ = playerDown.transform.position.z;
		Vector3 touchPosition = transform.position;
		float xRayValid = customCollide(xRayPoint);
		touchPosition.x = xRayValid;
		controller.transform.position = new Vector3(touchPosition.x,controller.transform.position.y,controller.transform.position.z); 
		playerDown.transform.position = new Vector3(touchPosition.x,playerPosY,playerPosZ); 
	}

	void ActivatePlayer()
	{
		playerDown.collider.enabled = true;
	}

	void DeactivatePlayer()
	{
		playerDown.collider.enabled = false;
	}

	//<--------------------------------------------------------------- End of Player Function ------------------------------------------------------------>//

	float customCollide(float x)
	{
		if(x < -2.0f)
		{
			x = -2.0f;
		}
		else if(x > 2.0f)
		{
			x = 2.0f;
		}
		return x;
	}
}
