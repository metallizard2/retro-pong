﻿using System.Collections.Generic;

namespace Assets.Scripts.Models
{
    public class MenuButtonModels : BaseModel
    {
        public readonly List<MenuButtonModel> List = new List<MenuButtonModel>(); 
    }
}
