﻿using UnityEngine;

namespace Assets.Scripts.Models
{
    public class MenuButtonModel : BaseModel
    {
        private const float DEFAULT_OUT_ANIMATIONN_DISTANCE = 6f;

        public string GameObjectName { get; set; }		//Block for gameobject identity
        public Vector3 GameObjectPosition { get; set; }	//--

        public string AxisAnimation { get; set; }		//Block for gameobject animation property
        public float DistanceIntroAnim { get; set; }	//--
        public Vector3 HighlightPosition { get; set; }

        public string TargetSceneId { get; set; }// Target scene to be loaded, if empty, then we exit the game
        public float DistanceOutAnimation { get; set; }

		public string TargetMenu{ get; set; }
        
        public bool FadeOther { get; set; }

        public MenuButtonModel(string name, Vector3 position, string axisAnimation, float distanceAnimation, Vector3 highlightPosition, string id, string targetSceneId, string targetMenu, bool fadeOther)
        {
            DistanceOutAnimation = DEFAULT_OUT_ANIMATIONN_DISTANCE;

            GameObjectName = name;
            GameObjectPosition = position;
            AxisAnimation = axisAnimation;
            DistanceIntroAnim = distanceAnimation;
            HighlightPosition = highlightPosition;
            Id = id;
            TargetSceneId = targetSceneId;

			TargetMenu = targetMenu;

            FadeOther = fadeOther;
        }
    }
}
