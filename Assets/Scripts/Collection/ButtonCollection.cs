﻿using System.Collections.Generic;
using Assets.Scripts.Controllers;
using Assets.Scripts.Models;

namespace Assets.Scripts.Collection
{
    public class ButtonCollection : BaseController
    {
        public readonly List<MenuButton> Children = new List<MenuButton>(); //contain data for creating button

        public ButtonCollection(MenuButtonModels model, BaseController parent, IContext context) : base(model, parent, context)
        {
            foreach (var menuButtonModel in model.List)
            {
                var menuButton = new MenuButton(menuButtonModel, this, context);
                Add(menuButton);
            }
        }

		public void DeactivateButton()
		{
			foreach(var menuButton in Children)
			{
				menuButton.DisableButton();
			}
		}

        public void Add(MenuButton newChild)
        {
            Children.Add(newChild);
        }

        public void Create()
        {
            foreach (var menuButton in Children)
            {
                menuButton.CreateMenuButton();
            }
        }

        public void IntroAnimation()
        {
            foreach (var menuButton in Children)
            {
                MainMenuContext.Instance.StartCoroutine(menuButton.IntroAnimation());
            }
        }

		public void OutroAnimation()
		{
			foreach (var menuButton in Children)
			{
				MainMenuContext.Instance.StartCoroutine(menuButton.OutroAnimation());
			}
		}

        public void StartOutAnimationExcept(MenuButton menuButtonToIgnore, bool isFadeEffect)
        {
            var distanceMultiplier = 1f;
            foreach (var button in Children)
            {
                if (button == menuButtonToIgnore)
                {
                    distanceMultiplier *= -1;
                    MainMenuContext.Instance.StartCoroutine(button.GotoTopAnimation());
                }
                else
				{
					if(isFadeEffect)
					{
						button.OutAnimation(distanceMultiplier * 0.33f);
						MainMenuContext.Instance.StartCoroutine(button.ToggleFade());
					}
					else
                    	button.OutAnimation(distanceMultiplier);
				}
            }
        }
    }
}
