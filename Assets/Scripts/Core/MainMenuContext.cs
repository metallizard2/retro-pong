﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Controllers;
using UnityEngine;
using Assets.Scripts;
using Assets.Scripts.Collection;
using Assets.Scripts.Models;
using Assets.Scripts.Views;

public class MainMenuContext : MonoBehaviour, IContext
{
    public static MainMenuContext Instance { get; private set; }

	Ray _ray;
	RaycastHit _rayCastHit;
    GameObject _cameraGameObject;

    private void Awake()
    {
        if (Instance != null)
            throw new Exception("Fail to instantiate a new MainMenuContext, a context has already been defined");

        Instance = this;
    }

	void Start()
	{
		Prepare();
	}

	void Prepare()
	{
		Label = Instantiate(Resources.Load("Prefabs/PlayerName")) as GameObject;
		Label.SetActive(false);

		Instantiate(Resources.Load("Prefabs/Background"));

		var mainCam = GameObject.Find("Main Camera");

	    if (!mainCam)
	    {
			var cameraInScene = FindObjectsOfType<Camera>();
			if(cameraInScene.Length > 0)
			{
				foreach(Camera deleteCamera in cameraInScene)
					DestroyObject(deleteCamera.gameObject);
			}
			mainCam = new GameObject("Main Camera");
			mainCam.transform.position = new Vector3(0, 0, -10);
			mainCam.tag = "MainCamera";
			mainCam.AddComponent<Camera>().camera.orthographic = true;
			mainCam.AddComponent<GUILayer>();
			mainCam.AddComponent("FlareLayer");
			mainCam.AddComponent<AudioListener>();
	    }

		_cameraGameObject = mainCam;
		var uiCamera = _cameraGameObject.AddComponent<UICamera>();
		uiCamera.Camera = _cameraGameObject.camera;
		uiCamera.Context = this;

		GameObject mainLight = new GameObject("Main Light");
		mainLight.transform.position = new Vector3(0, 10, -25);
		mainLight.AddComponent<Light>().light.type = LightType.Directional;
		mainLight.light.intensity = 0.4f;

	    var buttonModels = new MenuButtonModels();
		buttonModels.List.Add(new MenuButtonModel("1 Player", new Vector3(-1, 9.5f, 0), "y", -8.0f, new Vector3(-1, 3, 0), "1", "1P", ButtonBehavior.ChangeScene, "Submenu1P", true));
        buttonModels.List.Add(new MenuButtonModel("2 Player", new Vector3(7, .5f, 0), "x", -8.0f, new Vector3(-1, 0, 0), "2", "2P", ButtonBehavior.ChangeScene, "2PScene", false));
        buttonModels.List.Add(new MenuButtonModel("About", new Vector3(-1, -8.5f, 0), "y", 8.0f, new Vector3(-1, 4, 0), "3", null, ButtonBehavior.ChangeScene, "SubmenuAbout", false));
        buttonModels.List.Add(new MenuButtonModel("Exit", new Vector3(-9, -1.5f, 0), "x", 8.0f, new Vector3(-1, -1.5f, 0), "4", null, ButtonBehavior.Exit, null, false));

        Buttons = new ButtonCollection(buttonModels, null, this);
        Buttons.Create();
        Buttons.IntroAnimation();
    }

    public void Register(BaseController controller)
    {
        Controllers.Add(controller.Id, controller);
    }

    public T GetController<T>(string id)
    {
        // TODO: Implement getting controller from Controllers dictionary
    }

    #region Collections

    // Used to access a controller from another controller
    public readonly Dictionary<string, BaseController> Controllers = new Dictionary<string, BaseController>(); 

    public ButtonCollection Buttons;

    #endregion
}