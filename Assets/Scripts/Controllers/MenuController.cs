﻿using Assets.Scripts.Models;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class MenuController : BaseController
    {
        private readonly MenuModel _model;
        public MenuController(MenuModel model, BaseController parent, IContext context) : base(model, parent, context)
        {
            _model = model;
        }

        public void Show() { }
    }
}
