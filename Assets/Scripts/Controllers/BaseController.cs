﻿using Assets.Scripts.Models;

namespace Assets.Scripts.Controllers
{
	public class BaseController 
	{
		private BaseModel _model;
	    protected readonly BaseController Parent;
	    protected readonly IContext Context;

		public BaseController(BaseModel model, BaseController parent, IContext context)
		{
			_model = model;
		    Parent = parent;
		    Context = context;

		    Id = _model.Id;

		    Context.Register(this);
		}

	    public readonly string Id;
	}
}