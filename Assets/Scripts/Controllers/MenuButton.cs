﻿using System;
using System.Collections;
using UnityEngine;
using Assets.Scripts.Collection;
using Assets.Scripts.Models;
using Assets.Scripts.Views;

namespace Assets.Scripts.Controllers
{
    public class MenuButton : BaseController
    {
		private static bool AnimationEnded;
        private readonly MenuButtonModel _model;

        /// <summary>
        /// Constructs a new MenuButton instancce
        /// </summary>
        /// <param name="model">model containing all MenuButton data</param>
        /// <param name="parent">Parent of this Controller</param>
        public MenuButton(MenuButtonModel model, BaseController parent, IContext context) : base(model, parent, context)
        {
            _model = model;
        }

        private GameObject _menuBtnGO;
        private Collider _buttonCollider;

        public void CreateMenuButton()
        {
            _menuBtnGO = GameObject.Instantiate(Resources.Load("Prefabs/MainMenuButton")) as GameObject;
            _menuBtnGO.name = _model.GameObjectName;
            _menuBtnGO.GetComponent<TextMesh>().text = _model.GameObjectName;
            _menuBtnGO.transform.position = _model.GameObjectPosition;

            _menuBtnGO.AddComponent<OnClickActionBinding>().OnClickAction = OnHit;

            _buttonCollider = _menuBtnGO.GetComponent<BoxCollider>();
            if (_buttonCollider == null)
                _buttonCollider = _menuBtnGO.AddComponent<BoxCollider>();
        }

        public void DisableButton()
        {
            _buttonCollider.enabled = false;
        }

        #region Animation Methods
        public IEnumerator IntroAnimation()
        {
			MainMenuContext.Instance.CanEscape = false;
			var totalTime = 2.1f;
			var animTime = 2;
            iTween.MoveBy(_menuBtnGO,
                iTween.Hash(
                    _model.AxisAnimation, _model.DistanceIntroAnim,
                    "easeType", "easeInOutExpo",
                    "loopType", "none",
                    "delay", totalTime - animTime,
                    "time", animTime
                    ));
			yield return new WaitForSeconds(totalTime);
			ControlAfterAnimation();
        }

        public IEnumerator OutroAnimation()
        {
			MainMenuContext.Instance.CanEscape = false;
			var totalTime = .877f;
            var animTime = .777f;
            iTween.MoveBy(_menuBtnGO,
                iTween.Hash(
                    _model.AxisAnimation, -_model.DistanceIntroAnim,
                    "easeType", "easeInOutExpo",
                    "loopType", "none",
                    "delay", totalTime - animTime,
                    "time", animTime
                    ));

            yield return new WaitForSeconds(totalTime);
            Application.Quit();
            Debug.Log("Application has been quit !");
        }

        public void OutAnimation(float distanceMultiplier)
        {
			iTween.MoveBy(_menuBtnGO,
                iTween.Hash(
                    "y", _model.DistanceOutAnimation * distanceMultiplier,
                    "easeType", "easeInOutExpo",
                    "loopType", "none",
                    "delay", .1f,
                    "time", .777f
                    ));
        }

        public IEnumerator GotoTopAnimation()
        {
			MainMenuContext.Instance.CanEscape = false;
			var totalTime = 1.3f;
			var animTime = .777f;
			iTween.MoveTo(_menuBtnGO,
                iTween.Hash(
                    "position", _model.HighlightPosition,
                    "easeType", "easeInOutExpo",
                    "loopType", "none",
                    "delay", totalTime - animTime,
                    "time", animTime
                    ));
			yield return new WaitForSeconds(totalTime);
			ControlAfterAnimation();
			AnimationEnded = true;
        }

        #endregion

        private bool _isFaded;

        public IEnumerator ToggleFade()
        {
            var totalTime = 1.25f;
            var animTime = 0.25f;
            iTween.FadeTo(_menuBtnGO, iTween.Hash("alpha", _isFaded ? 1 : 0.1f, "delay", totalTime - animTime, "time", animTime));
            _isFaded = !_isFaded;

            yield return new WaitForSeconds(totalTime);
        }

        void ControlAfterAnimation()
        {
			MainMenuContext.Instance.CanEscape = true;
        }

        private void OnHit()
        {
            var parentButtonCollection = Parent as ButtonCollection;

            if (parentButtonCollection == null)
                throw new Exception(string.Format("Failed to invoke OnHit on {0}: {1}, {0} has to be a child of ButtonCollection", GetType(), _model.Id));

            parentButtonCollection.DeactivateButton();

            if (!string.IsNullOrEmpty(_model.TargetSceneId))
            {
                parentButtonCollection.StartOutAnimationExcept(this, _model.FadeOther);
                Context.GetController<MenuController>(_model.TargetSceneId).Show();
            }
            else
            {
                // TargetSceneId is empty, this means we exit the game
                parentButtonCollection.OutroAnimation();
            }
        }
    }
}