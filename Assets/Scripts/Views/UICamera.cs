﻿using UnityEngine;

namespace Assets.Scripts.Views
{
    public class UICamera : MonoBehaviour
    {
        public Camera Camera;

		private string _playerName; // TODO: Explain why this is needed here, or move it to another place
		private string about;

        private class MouseButton
        {
            public Collider LastCollider;
        }
        private readonly MouseButton _leftMouseButton = new MouseButton();
        public IContext Context { get; set; }

        private void Start()
		{
            // TODO: Should not be here, this should be it's own scene
            //_playerName = "RetroPong Player";
            //about = "This is PONG games ! \n\n " +
            //    "You miss your time when you play pong in\n arcade ?" +
            //        "don't worry we'll give you those\n memories !\n\n" +
            //        "Press the oval to summon your player\nand move it to left or right.\n" +
            //        "Don't let the ball pass and get your name\nin glorious highscore !\n\n\n" +
            //        "There's 2Player DeadMatch Mode too !\ntest your concentration with your friend.\n" +
            //        "whoever let the ball passes first\nis a loser !\n\n\n\n" +
            //        "Created By :\nAnjar Aditya Pratama (@adittt_tttida)\nFacebook.com/bloodyadit\n\n\n\n" +
            //        "Special Thanks to:\nITween Library\nwww.1001freefonts.com/\nRifal Mafaza (@rivalfaza)";
		}

        private void Update()
        {
            var mousePosition = Input.mousePosition;
            var ray = Camera.ScreenPointToRay(new Vector3(mousePosition.x, mousePosition.y, 0));
            RaycastHit hitInfo;

            if (Input.GetMouseButtonDown(0))
			{
                if (Physics.Raycast(ray.origin, ray.direction, out hitInfo, 100f))
				{
                    _leftMouseButton.LastCollider = hitInfo.collider;
				}
			}

            if (Input.GetMouseButtonUp(0))
            {
                if (Physics.Raycast(ray.origin, ray.direction, out hitInfo, 100f))
				{
                    if (_leftMouseButton.LastCollider == hitInfo.collider)
                        hitInfo.collider.GetComponent<OnClickActionBinding>().OnClickAction();
				}

                _leftMouseButton.LastCollider = null;
            }

			if(Input.GetKeyDown(KeyCode.Escape))
			{
                // TODO: Move the logic to MenuController
				if(MainMenuContext.Instance.CanEscape) Context.OnEscape();
			}
        }

        // TODO: Explain why this is needed here, or move it to another place
		private void OnGUI()
		{
			if(Context.Label.activeSelf)
			{
				_playerName = GUI.TextField(new Rect(Screen.width/3.2f,Screen.height*0.35f,Screen.width*0.4f,Screen.height*0.06f), _playerName,15);
				if(GUI.Button(new Rect(Screen.width/2.6f,Screen.height*0.45f,Screen.width*0.25f,Screen.height*0.05f), "Play"))
				{
                    // TODO: Move the logic to MenuController
					Application.LoadLevel("1P");
				}
			}
			if(Context.ShowAbout)
			{
				GUI.Label(new Rect(Screen.width/8.0f,Screen.height/6.0f,Screen.width/1.3f,Screen.height/1.3f), about, "TextField");
			}
		}
    }
}
