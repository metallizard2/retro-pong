﻿using System;
using UnityEngine;

namespace Assets.Scripts.Views
{
    public class OnClickActionBinding : MonoBehaviour
    {
        public Action OnClickAction;
    }
}
