﻿using UnityEngine;

namespace Assets.Scripts.Views
{
	public class BallAnimController : MonoBehaviour
	{
		private Transform _ballTransform;

		void Start()
		{
			_ballTransform = transform;
		}

		void Update()
		{
			_ballTransform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime * 1.5f);
		}
	}
}