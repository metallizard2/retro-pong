﻿using Assets.Scripts.Controllers;

namespace Assets.Scripts
{
    public interface IContext
    {
        void Register(BaseController controller);

        T GetController<T>(string id);
    }
}
